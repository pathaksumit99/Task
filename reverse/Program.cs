﻿using System;

class Program
{
    static void Main()
    {
        Console.Write("Enter a string to reverse: ");
        string input = Console.ReadLine();

        string reversed = "";
        foreach (char c in input)
        {
            reversed = c + reversed;
        }

        Console.WriteLine("Reversed string: " + reversed);
    }
}

