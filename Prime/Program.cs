﻿using System;

public class Check
{
    static bool prime(int num)
    {
        // Loop to check for factors of the number
        for (int i = 2; i < num; i++)
        {
            if (num % i == 0)
            {
                return false;
            }
        }
        return true;
    }

    public static void Main()
    {
        Console.Write("Input a number : ");
        int n = Convert.ToInt32(Console.ReadLine());
        if (prime(n))
        {
            Console.WriteLine(n + " is a prime number");
        }
        else
        {
            Console.WriteLine(n + " is not a prime number");
        }
    }
}
