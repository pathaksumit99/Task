﻿using System;

class Program
{
    static void Main()
    {
        int sumEven = 0;
        for (int i = 2; i <= 50; i += 2)
        {
            sumEven += i;
        }

        Console.WriteLine("Sum of even numbers from 1 to 50: " + sumEven);
    }
}
